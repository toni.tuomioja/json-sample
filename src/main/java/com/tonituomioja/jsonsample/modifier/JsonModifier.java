package com.tonituomioja.jsonsample.modifier;

import com.tonituomioja.jsonsample.JsonOperation;

/**
 * Represents an operation which modifies JSON sample
 *
 * {@link JsonModifier} name should be fluent and easily readable.
 *
 * <code>
 *     var personLivingAbroad = PERSON.with(foreignStreetAddress);
 * </code>
 */
@FunctionalInterface
public interface JsonModifier {
    /**
     * Modifies the <code>jsonSample</code>
     *
     * @param json
     */
    void modify(JsonOperation json);

    /**
     * Combines two {@link JsonModifier}s into a single modifier
     *
     * @param modifier
     * @return
     */
    default JsonModifier and(JsonModifier modifier) {
        return json-> {
            this.modify(json);
            modifier.modify(json);
        };
    }
}
