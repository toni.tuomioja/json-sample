package com.tonituomioja.jsonsample.modifier;

import com.tonituomioja.jsonsample.JsonSample;
import com.tonituomioja.jsonsample.property.PropertySpec;
import org.json.JSONArray;

import java.util.stream.IntStream;

/**
 * Commonly needed {@link JsonModifier}s
 * <p>
 * The naming of the modifiers aims to make them fluent and easily readable when passed into {@link JsonSample#with(JsonModifier...) }.
 *
 * <p>
 * E.g. instead of {@code var personWithoutStreetAddress = person.with(removeProperty(streetAddress);}, the following is more preferable
 * {@code var personWithoutStreetAddress = person.with(missing(streetAddress);}
 */
public class JsonModifiers {

    /**
     * Returns a modifier, which empties the <code>arrayProperty</code>
     *
     * @param arrayProperty
     * @return
     */
    public static final JsonModifier emptyArrayOf(PropertySpec<JSONArray> arrayProperty) {
        return json -> {
            final var array = json.get(arrayProperty);
            IntStream.range(0, array.length()).forEach(array::remove);
        };
    }

    /**
     * Returns a modifier, which puts an empty {@link String} as a value for the {@code stringProperty}
     *
     * @param stringProperty
     * @return
     */
    public static final JsonModifier emptyStringAs(PropertySpec<String> stringProperty) {
        return json -> json.put(stringProperty, "");
    }

    /**
     * Returns a modifier. which removes the {@code property}
     *
     * @param property
     * @return
     */
    public static final JsonModifier missing(PropertySpec<?> property) {
        return json -> json.remove(property);
    }
}
