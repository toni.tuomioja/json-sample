package com.tonituomioja.jsonsample;

import com.tonituomioja.jsonsample.modifier.JsonModifier;
import com.tonituomioja.jsonsample.property.PropertySpec;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A wrapper for a {@link JSONObject} to make dealing with JSON documents more fluent and easy in tests such as Web API tests.
 */
public class JsonSample implements JsonAccessOperation {

    /**
     * Constructs a new instance of {@link JsonSample} by reading a JSON file from classpath
     *
     * @param classpathFilename
     * @return
     * @throws IllegalArgumentException if the JSON file was not found
     */
    public static JsonSample ofClasspathFile(String classpathFilename) {
        Objects.requireNonNull(classpathFilename, "classpathFilename is required");

        final var is = JsonSample.class.getClassLoader().getResourceAsStream(classpathFilename);

        if (is == null) {
            throw new IllegalArgumentException("Could not find a JSON file from classpath with the filename " + classpathFilename);
        }

        return new JsonSample(new InputStreamReader(is, StandardCharsets.UTF_8));
    }

    public static final JsonSample EMPTY_OBJECT_LITERAL = new JsonSample(new StringReader("{}"));

    private Reader reader;

    protected JSONObject cachedJsonObject;

    private JsonSample(JSONObject jsonObject) {
        this.cachedJsonObject = new JSONObject(jsonObject.toString());
    }

    public JsonSample(Reader reader) {
        this.reader = reader;
    }

    /**
     * Creates a copy of this sample and applies the changes made by the {@code modifiers} to the copy
     *
     * @param modifiers modifiers which modify the copy
     * @return the modified copy of this sample
     */
    public JsonSample with(JsonModifier... modifiers) {
        final var copy = new MutableJsonSample(loadIfNeeded());

        try {
            Arrays.stream(modifiers).forEach(modifier -> {
                modifier.modify(copy);
            });
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        return copy;
    }

    /**
     * Returns this sample as a {@link String}
     *
     * @return
     */
    public String asJsonAsString() {
        loadIfNeeded();
        return cachedJsonObject.toString();
    }

    /**
     * Returns this sample as a {@link JSONObject}
     *
     * @return
     */
    public JSONObject asJsonObject() {
        loadIfNeeded();
        return new JSONObject(asJsonAsString());
    }

    /**
     * Returns the value of the {@code property}
     *
     * @param property
     * @param <E>
     * @return
     */
    public <E> E get(PropertySpec<E> property) {
        loadIfNeeded();
        return property.getPropertyValue(cachedJsonObject);
    }

    /**
     * Returns {@code true} if the property exists
     *
     * @return
     */
    public boolean exists(PropertySpec<?> property) {
        return property.exists(cachedJsonObject);
    }

    private JSONObject loadIfNeeded() {
        if (cachedJsonObject == null) {

            try (BufferedReader bufferedReader = new BufferedReader(reader)) {
                String json = bufferedReader.lines().collect(Collectors.joining());

                cachedJsonObject = new JSONObject(json);

            } catch (IOException e) {
                throw new RuntimeException("Failed to read JSON", e);
            }
        }

        return cachedJsonObject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JsonSample sample = (JsonSample) o;
        return asJsonAsString().equals(asJsonAsString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(cachedJsonObject);
    }

    /**
     * Mutable version of {@link JsonSample}
     *
     */
    public final static class MutableJsonSample extends JsonSample implements JsonOperation {

        /**
         * Creates a copy of {@link JSONObject}
         *
         * @param jsonObject
         */
        private MutableJsonSample(JSONObject jsonObject) {
            super(new JSONObject(jsonObject.toString()));
        }



        @Override
        public <E> JSONObject put(PropertySpec<E> property, E value) {
            property.setPropertyValue(cachedJsonObject, value);
            return cachedJsonObject;
        }

        @Override
        public <E> void remove(PropertySpec<E> property) {
            property.remove(cachedJsonObject);
        }
    }
}
