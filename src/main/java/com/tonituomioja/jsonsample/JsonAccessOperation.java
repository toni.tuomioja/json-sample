package com.tonituomioja.jsonsample;

import com.tonituomioja.jsonsample.property.PropertySpec;

/**
 * An operation for accessing properties of a {@link JsonSample}
 */
public interface JsonAccessOperation {
     <E> E get(PropertySpec<E> spec);
}
