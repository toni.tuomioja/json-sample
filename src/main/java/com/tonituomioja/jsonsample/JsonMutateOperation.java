package com.tonituomioja.jsonsample;

import com.tonituomioja.jsonsample.property.PropertySpec;
import org.json.JSONObject;

/**
 * An operation for mutating properties of a JSON sample
 */
public interface JsonMutateOperation {
    <E> JSONObject put(PropertySpec<E> property, E value);

    <E> void remove(PropertySpec<E> property);
}
