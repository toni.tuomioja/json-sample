package com.tonituomioja.jsonsample.property;

import org.json.JSONArray;
import org.json.JSONObject;

public class BasicPropertySpecs {
    public static PropertySpec<Integer> _integer(String property) {
        return new PropertySpec<>(property, JSONObject::getInt, JSONObject::put);
    }

    public static PropertySpec<Long> _long(String property) {
        return new PropertySpec<>(property, JSONObject::getLong, JSONObject::put);
    }

    public static PropertySpec<String> _string(String property) {
        return new PropertySpec<>(property, JSONObject::getString, JSONObject::put);
    }

    public static PropertySpec<JSONArray> jsonArray(String property) {
        return new PropertySpec<>(property, JSONObject::getJSONArray, JSONObject::put);
    }

    public static JsonObjectPropertySpec jsonObject(String property) {
        return new JsonObjectPropertySpec(property);
    }

    public static PropertySpec<Object> any(String property) {
        return new PropertySpec<>(property, JSONObject::get, JSONObject::put);
    }

}
