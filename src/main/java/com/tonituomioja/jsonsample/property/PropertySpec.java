package com.tonituomioja.jsonsample.property;

import org.json.JSONObject;

import java.util.function.Function;

/**
 * Specification of a JSON object literal property
 *
 * @param <T>
 */
public class PropertySpec<T> {
    /**
     * Name of the property
     */
    protected final String property;

    /**
     * Accessor for accessing the value of the property
     */
    protected Accessor<T> accessor;

    /**
     * Mutator for modifying the value of the property
     */
    protected Mutator<T> mutator;

    public PropertySpec(String property, Accessor<T> accessor) {
        this(property, accessor, (json, prop, val) -> {});
    }

    public PropertySpec(String property, Accessor<T> accessor, Mutator<T> mutator) {
        this.property = property;
        this.accessor = accessor;
        this.mutator = mutator;
    }

    /**
     * Returns true if the property exists in {@code jsonObjectLiteral}
     *
     * @param jsonObjectLiteral
     * @return
     */
    public boolean exists(JSONObject jsonObjectLiteral) {
        return jsonObjectLiteral.has(property);
    }

    /**
     * Returns the property value of {@code jsonObjectLiteral}
     *
     * @param jsonObjectLiteral
     * @return
     */
    public T getPropertyValue(JSONObject jsonObjectLiteral) {
        return accessor.apply(jsonObjectLiteral, property);
    }

    /**
     * Sets the property value of {@code jsonObjectLiteral}
     *
     * @param jsonObjectLiteral
     * @param value
     */
    public void setPropertyValue(JSONObject jsonObjectLiteral, T value) {
        mutator.accept(jsonObjectLiteral, property, value);
    }

    /**
     * Removes the property from <code>jsonObjectLiteral</code>
     *
     * @param jsonObjectLiteral
     */
    public void remove(JSONObject jsonObjectLiteral) {
        jsonObjectLiteral.remove(property);
    }

    @FunctionalInterface
    public interface Accessor<T> {
        T apply(JSONObject jsonObjectLiteral, String property);
    }

    @FunctionalInterface
    public interface Mutator<T> {
        void accept(JSONObject jsonObjectLiteral, String property, T value);
    }
}
