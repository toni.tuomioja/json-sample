package com.tonituomioja.jsonsample.property;

import org.json.JSONObject;

/**
 * Specification of a JSON object literal property
 */
public class JsonObjectPropertySpec extends PropertySpec<JSONObject> {

    public JsonObjectPropertySpec(String property) {
        super(property, JSONObject::getJSONObject, JSONObject::put);
    }

    public <E> PropertySpec<E> andThen(PropertySpec<E> spec) {
        return new PropertySpec<>(property,
                (json, prop) -> spec.getPropertyValue(getPropertyValue(json)),
                (json, prop, val) -> spec.setPropertyValue(getPropertyValue(json), val));
    }
}
