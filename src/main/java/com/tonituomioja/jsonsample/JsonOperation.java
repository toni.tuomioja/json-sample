package com.tonituomioja.jsonsample;

/**
 * Operations for accessing and mutating properties of a JSON sample
 */
public interface JsonOperation extends JsonAccessOperation, JsonMutateOperation {
}
