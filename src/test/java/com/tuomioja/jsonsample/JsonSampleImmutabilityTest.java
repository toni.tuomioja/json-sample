package com.tuomioja.jsonsample;

import com.tonituomioja.jsonsample.*;
import com.tonituomioja.jsonsample.modifier.JsonModifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.StringReader;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static com.tonituomioja.jsonsample.property.BasicPropertySpecs.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static com.tonituomioja.jsonsample.JsonSample.EMPTY_OBJECT_LITERAL;

/**
 * Verifies that {@link JsonSample} cannot be mutated neither indirectly by first calling {@link JsonAccessOperation} returning a mutable object, or directly by calling
 * {@link JsonMutateOperation} within a {@link JsonModifier}
 */
public class JsonSampleImmutabilityTest {

    private static Consumer<JsonSample> mutator(Consumer<JsonSample> mutateOperation) {
        return mutateOperation;
    }

    private static JsonSample jsonSample(String json) {
        return new JsonSample(new StringReader(json));
    }

    @Test
    @DisplayName("JsonSample#with is called, the JsonSample instance passed into JsonModifier, is not the same instance as the one which the method was called with")
    void with__JsonSamplePassedIntoJsonModifier__SampleIsNotTheOriginal() {
        EMPTY_OBJECT_LITERAL.with(json -> assertFalse(EMPTY_OBJECT_LITERAL == json, "Original instance should not be passed into JsonModifier"));
    }

    public static Stream<Arguments> getIndirectlyMutatingOperations() {
        final var anyObjectLiteral = jsonSample("{\"foo\":\"bar\"}");
        final var anyArray = jsonSample("{\"foo\":[\"bar\"]}");
        final var anyObjectLiteralWithObjectProperty = jsonSample("{\"foo\":{\"bar\":\"baz\"}}");

        return Stream.of(
                arguments("toJsonAsObject", anyObjectLiteral, mutator(sample -> sample.asJsonObject().put("foo", "bar"))),
                arguments("getJsonArray", anyArray, mutator(sample -> sample.get(jsonArray("foo")).remove(0))),
                arguments("with", anyObjectLiteral, mutator(sample -> sample.with(json -> json.put(_string("bar"), "baz")))),
                arguments("getJsonObject", anyObjectLiteralWithObjectProperty, mutator(sample -> sample.get(jsonObject("foo")).remove("bar")))
        );
    }

    @MethodSource("getIndirectlyMutatingOperations")
    @DisplayName("Given JsonSample is accessed by an accessor method, when the mutable return value is modified, then the JsonSample remains unmodified")
    @ParameterizedTest(name = "accessor method {0}")
    void jsonIsAccessedByAnAccessorMethod__AnyModificationIsPerformedOnJson__OriginalJsonRemainsUnmodified(String mutatorName, JsonSample sample, Consumer<JsonSample> mutator) {
        final var expected = sample.asJsonAsString();

        mutator.accept(sample);

        assertEquals(expected, sample.asJsonAsString());
    }

    @DisplayName("When JSON is modified directly within a JsonModifier, the original sample remains unmodified")
    void jsonIsMutatedWithinAModifier__AnyModificationPerformedOnJson__OriginalJsonRemainsUnmodified() {
        final var json = "{}";
        final var sample = new JsonSample(new StringReader(json));

        sample.with(j -> j.put(_string("foo"), "bar"));

        assertEquals(json, sample.asJsonAsString());
    }
}
