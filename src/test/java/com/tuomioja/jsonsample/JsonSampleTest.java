package com.tuomioja.jsonsample;

import com.tonituomioja.jsonsample.JsonSample;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.tonituomioja.jsonsample.property.BasicPropertySpecs._string;
import static com.tonituomioja.jsonsample.JsonSample.EMPTY_OBJECT_LITERAL;
import static org.junit.jupiter.api.Assertions.*;

public class JsonSampleTest {

    @Test
    void ofClasspathFile__FileDoesExist__DoesNotThrowException() {
        assertDoesNotThrow(() -> JsonSample.ofClasspathFile("person.json"));
    }

    @Test
    void ofClasspathFile__FileDoesNotExist__DoesThrowException() {
        assertThrows(IllegalArgumentException.class, () -> JsonSample.ofClasspathFile("nonexistingfile.json"));
    }

    @Test
    void ofClasspathFile__JsonArray__DoesThrowException() {
        JsonSample.ofClasspathFile("persons.json");
    }

    @Test
    void asJsonObject__emptyObjectLiteral() {
        assertEquals(new JSONObject("{}"), EMPTY_OBJECT_LITERAL.asJsonObject());
    }

    @Test
    void asJsonString__emptyObjectLiteral() {
        assertEquals("{}", EMPTY_OBJECT_LITERAL.asJsonAsString());
    }

    @Test
    @DisplayName("JsonSample#with is called with a JsonModifier which modifies the original sample, then the modified sample is returned")
    void with__JsonSamplePassedToJsonModifierIsModified__ModifiedSampleIsReturned() {
        final var objectLiteralWithFooProperty = EMPTY_OBJECT_LITERAL.with(json -> json.put(_string("foo"), "bar"));

        assertEquals("{\"foo\":\"bar\"}", objectLiteralWithFooProperty.asJsonAsString());
    }
}
