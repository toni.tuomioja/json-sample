package com.tuomioja.jsonsample;

import com.tonituomioja.jsonsample.property.BasicPropertySpecs;
import com.tonituomioja.jsonsample.property.JsonObjectPropertySpec;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 *
 * Nested property refers to any object property of type JSON e.g.
 * <code>
 *     {
 *         "foo": {
 *             "bar: "baz"
 *         }
 *     }
 * </code>
 *
 */
@DisplayName("Given an instance of JsonObjectPropertySpec and JSON object literal with a nested property")
public class JsonObjectPropertySpecTest {

    @Test
    @DisplayName("when nestedProperty.getPropertyValue is called, then the nested property value is returned")
    void getPropertyValue_ObjectPropertyExist_NestedPropertyExist__PropertyValueIsReturned() {
        final var jsonObject = new JSONObject().put("foo", new JSONObject().put("bar", "baz"));

        final var fooBarProperty = new JsonObjectPropertySpec("foo").andThen(BasicPropertySpecs._string("bar"));

        assertEquals("baz", fooBarProperty.getPropertyValue(jsonObject));
    }

    @Test
    @DisplayName("when nestedProperty.getPropertyValue is called, and the nestedProperty does not exist, then exception is thrown")
    void getPropertyValue__nestedPropertyNotExist__Throws() {
        final var jsonObject = new JSONObject().put("foo", new JSONObject());
        final var fooBarProperty = new JsonObjectPropertySpec("foo").andThen(BasicPropertySpecs._string("bar"));

        assertThrows(JSONException.class, () -> fooBarProperty.getPropertyValue(jsonObject));
    }

    @Test
    @DisplayName("when nestedProperty.setPropertyValue is called, and the nestedProperty exist, then the nested property value is set")
    void setPropertyValue__nestedPropertyExist__NestedPropertyValueIsSet() {
        final var jsonObject = new JSONObject().put("foo", new JSONObject());

        final var fooBarProperty = new JsonObjectPropertySpec("foo").andThen(BasicPropertySpecs._string("bar"));

        fooBarProperty.setPropertyValue(jsonObject, "qux");

        assertEquals("qux", jsonObject.getJSONObject("foo").getString("bar"));
    }

}
