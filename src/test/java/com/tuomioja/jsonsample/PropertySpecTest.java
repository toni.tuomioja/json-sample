package com.tuomioja.jsonsample;

import com.tonituomioja.jsonsample.property.PropertySpec;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Specification of how {@link PropertySpec} should behave
 *
 * Written in the format of Given-When-Then
 */
@DisplayName("Given an instance of PropertySpec")
public class PropertySpecTest {

    @Nested
    @DisplayName("and getPropertyValue is called")
    class GetPropertyValue {

        @Test
        @DisplayName("when the property exists in the JSON document, then the property value matches the expected")
        void propertyExist__ReturnValueEqualsToPropertyValue() {
            final var property = "foo";
            final var expected = "bar";
            final var jsonDocument = new JSONObject().put(property, expected);

            final var fooProperty = new PropertySpec<>(property, JSONObject::getString);

            assertEquals(expected, fooProperty.getPropertyValue(jsonDocument));
        }

        @Test
        @DisplayName("when the property exists, but the type does not match with the property spec, then exception is thrown")
        void propertyExist_PropertyTypeDoesNotMatchPropertySpecType__ExceptionIsThrown() {
            final var property = "foo";
            final var propertyValue = "bar";
            final var jsonDocument = new JSONObject().put(property, propertyValue);

            final var fooProperty = new PropertySpec<>(property, JSONObject::getInt);

            assertThrows(JSONException.class, () -> fooProperty.getPropertyValue(jsonDocument));
        }

    }

    @Test
    @DisplayName("when called with parameters jsonDocument and propertyValue, then the jsonDocument contains the property and the value equals to propertyValue")
    void setPropertyValue__JsonPropertyValueMatches() {
        final var property = "foo";
        final var propertyValue = "bar";
        final var jsonObject = new JSONObject().put(property, propertyValue);

        final var fooProperty = new PropertySpec<>(property, JSONObject::getString, JSONObject::put);

        fooProperty.setPropertyValue(jsonObject, propertyValue);

        assertEquals(propertyValue, jsonObject.getString(property));
    }

    @Test
    @DisplayName("when remove is called with parameter jsonDocument, then the property is removed from the jsonDocument")
    void remove__PropertyExist__PropertyNoLongerExist() {
        final var property = "foo";
        final var jsonObject = new JSONObject();
        final var fooProperty = new PropertySpec<>(property, JSONObject::getString, JSONObject::put);
        fooProperty.setPropertyValue(jsonObject, "bar");

        assert jsonObject.has(property);

        fooProperty.remove(jsonObject);

        assertFalse(jsonObject.has(property));
    }

    @Test
    @DisplayName("when remove is called and the property does not exist, then an exception is not thrown")
    void remove__PropertyDoesNotExist__DoesNotThrow() {
        final var property = "foo";
        final var fooProperty = new PropertySpec<>(property, JSONObject::getString, JSONObject::put);

        assertDoesNotThrow(() -> fooProperty.remove(new JSONObject()));
    }
}
