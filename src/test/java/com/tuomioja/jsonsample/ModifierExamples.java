package com.tuomioja.jsonsample;

import com.tonituomioja.jsonsample.modifier.JsonModifier;
import com.tonituomioja.jsonsample.JsonSample;
import com.tonituomioja.jsonsample.property.PropertySpec;
import org.junit.jupiter.api.Test;

import static com.tonituomioja.jsonsample.property.BasicPropertySpecs.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static com.tonituomioja.jsonsample.modifier.JsonModifiers.*;
import static com.tonituomioja.jsonsample.JsonSample.ofClasspathFile;

public class ModifierExamples {

    private static final JsonSample PERSON = ofClasspathFile("person.json");

    public static final PropertySpec<Integer> AGE = _integer("age");

    public static final PropertySpec<String> STREET_ADDRESS = _string("streetAddress");

    @Test
    void modifyExistingProperty() {
        final JsonModifier age30OrOver = json -> json.put(_integer("age"), 30);
        assertTrue(PERSON.with(age30OrOver).get(AGE) >= 30);
    }

    @Test
    void removeExistingProperty() {
        PERSON.with(missing(STREET_ADDRESS));
    }

    @Test
    void replacingStringPropertyWithAnEmptyString() {
        PERSON.with(emptyStringAs(_string("name")));
    }

    @Test
    void combineMultipleJsonModifiersIntoOne() {
        final JsonModifier ageOver30 = json -> json.put(AGE, 37);
        final JsonModifier hasChildren = json -> json.get(jsonArray("children")).put(PERSON.asJsonObject());

        final var ageOf30OrOverAndHasChildren = ageOver30.and(hasChildren);

        PERSON.with(ageOf30OrOverAndHasChildren);
    }

    @Test
    void clearArrayItems() {
        PERSON.with(emptyArrayOf(jsonArray("phoneNumbers")));
    }

}
