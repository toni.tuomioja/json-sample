package com.tuomioja.jsonsample;

import com.tonituomioja.jsonsample.JsonSample;
import com.tonituomioja.jsonsample.modifier.JsonModifier;
import com.tonituomioja.jsonsample.property.JsonObjectPropertySpec;
import com.tonituomioja.jsonsample.property.PropertySpec;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.tonituomioja.jsonsample.JsonSample.ofClasspathFile;
import static com.tonituomioja.jsonsample.modifier.JsonModifiers.missing;
import static com.tonituomioja.jsonsample.property.BasicPropertySpecs._integer;
import static com.tonituomioja.jsonsample.property.BasicPropertySpecs.jsonObject;
import static org.junit.jupiter.api.Assertions.*;

public class JsonSampleExamplesTest {

    private static final PropertySpec<Integer> AGE = _integer("age");
    public static final JsonObjectPropertySpec ADDRESS = jsonObject("address");

    private static JsonModifier ageOf(int age) {
        return (json) -> json.put(AGE, age);
    }

    private JsonSample person;

    @BeforeEach
    void loadPerson() {
        person = ofClasspathFile("person.json");
    }

    @DisplayName("Using a missing modifier to remove a property")
    @Test
    void removingProperty() {
        assert person.get(ADDRESS) != null;

        final var missingStreetAddress = person.with(missing(ADDRESS));

        assertFalse(missingStreetAddress.exists(ADDRESS));
    }

    @DisplayName("Using a modifier to replace existing value")
    @Test
    void replacingPropertyWithInvalidValue() {
        final var anyInvalidAge = -1;
        assert person.get(AGE) != anyInvalidAge;

        final var personWithInvalidAge = person.with(ageOf(anyInvalidAge));

        assertEquals(anyInvalidAge, personWithInvalidAge.get(AGE));
    }

    @DisplayName("Combining modifiers to prevent code duplication")
    @Test
    void combiningModifiers() {
        final var anyInvalidAge = -1;
        assert person.exists(ADDRESS) && person.get(AGE) != anyInvalidAge;

        final var missingStreetAddressAndHasInvalidAge = person.with(missing(ADDRESS).and(ageOf(anyInvalidAge)));

        assertAll(
                () -> assertFalse(missingStreetAddressAndHasInvalidAge.exists(ADDRESS)),
                () -> assertEquals(anyInvalidAge, missingStreetAddressAndHasInvalidAge.get(AGE))
        );
    }
}
